'''
	by Paulo Inca <pauloginca@hotmail.com>
	
	MIT Style License
'''

from Queue import Queue

class Node:
	
	'''
		Node class defines the nodes, where which node
		has a value, a left and a right subtree.
	'''

	def __init__(self, key):
		self.left  = None
		self.right = None
		self.key   = key

	def is_leaf(self):
	
		'''
			is_leaf method checks if a node hasn't left and right
			subtree, meaning it is a leaf.
		'''

		return self.left == None and self.right == None

	def has_left_child(self):

		'''
			has_left_child method checks if a node has left subtree.
		'''

		return self.left != None

	def has_right_child(self):

		'''
			has_right_child method checks if a node has right subtree.
		'''

		return self.right != None

class Tree:

	'''
		Tree class defines the trees, where which tree has a root.

		Methos implemented in this class are: insert, search,
		printTree (pre_order, in_order, pos_order), ...
	'''
	
	def __init__(self):
		self.root = None

	def insert(self, key):

		'''
			insert method takes as parameter the key of the node
			to be inserted.

			if the very first node is to be inserted, it is
			done in the root, otherwise _insert() is called.
		'''

		if self.root == None:
			self.root = Node(key)
		else:
			self._insert(self.root, key)

	def _insert(self, node, key):

		'''
			_insert method takes as parameters the current node in
			the tree to be compared with the key and the key itself.

			if the key is less than the current node.key and node.left
			has no subtree alocated, then add the new node at this proint,
			else call _insert recursively for the left subtree.
			if the key is greater than the current node.key and node.right
			has no subtree alocated, then add the new node a this proint,
			else call _insert recursively for the right subtree.
			Otherwise, the key is duplicated, and it won't be inserted.
		'''

		if key < node.key:
			if node.left == None:
				node.left = Node(key)
			else:
				self._insert(node.left, key)
		elif key > node.key:
			if node.right == None:
				node.right = Node(key)
			else:
				self._insert(node.right, key)
		else:
			pass
	
	def search(self, node, key):

		'''
			search method checks if the key, passed by paramenter, is
			less than node.key of the current node. If true calls _search
			for the left subtree, else call _insert for the right subtree.
			
			The base is when key is equal to node.key, consequently the
			node is in the tree. If node is equal to None, the key isn't
			in the tree. 
		'''
	
		if node == None:
			return False
		if key == node.key:
			return True
		else:
			if key < node.key:
				return self.search(node.left, key)
			else:
				return self.search(node.right, key)

	def preorder(self, node):

		'''
			preorder method prints the current node key, then calls 
			preorder for the left subtree, and finally calls 
			preorder for the right subtree.
		'''

		if node != None:
			print node.key
			self.preorder(node.left)
			self.preorder(node.right)

	def inorder(self, node):

		'''
			inorder method calls inorder for the left subtree,
			then prints the current node key, and finally calls
			inorder for the right subtree. 
		'''

		if node != None:
			self.inorder(node.left)
			print node.key
			self.inorder(node.right)

	def posorder(self, node):

		'''
			posorder method calls posorder for the left subtree,
			then calls posorder for the right subtree, and finally
			prints the current node key.
		'''

		if node != None:
			self.posorder(node.left)
			self.posorder(node.right)
			print node.key

	def bfs(self):
		
		'''
			A call to this method results in a list of nodes traversed
			by breadth first.
		'''

		if self.root == None:
			print "Empty tree, nothing to be walked"
			return

		queue = Queue()
		queue.put(self.root)

		visited = []

		while not queue.empty():
			node = queue.get()
			visited.append(node.key)				
			if node.has_left_child():	
				queue.put(node.left)
			if node.has_right_child():				
				queue.put(node.right)

		return visited
